import { shallow } from "enzyme";
import * as React from "react";
import Editor from "./editor";

describe("the <Editor/> element", () => {
  it("should render ok", () => {
    let scene = {elements: []};
    let wrapper = shallow(<Editor scene={scene}/>);

    expect(wrapper).toBeTruthy();
  });
});