import * as React from "react";
import { KeyboardEvent } from "react";
import { Scene } from "../model/scene";
import { SceneNode } from "./scene";

// TODO: add a static version of this canvas for use in a locked final output mode?
// TODO: connect the state store to redux; manage state as a chain of reducers?

/** A canvas wrapper; manages the fabricJS canvas and DOM canvas elements. */
export default class Canvas extends React.Component<{ scene: Scene, width: number, height: number }> {
  element: HTMLCanvasElement | null;
  canvas: fabric.Canvas;

  constructor() {
    super();

    this.onKeyUp = this.onKeyUp.bind(this);
  }

  componentDidMount() {
    // build the main fabric canvas; wrap the referenced DOM element.
    if (!this.element) {
      throw new Error("Unable to locate canvas element.");
    }
    this.canvas = new fabric.Canvas(this.element);
  }

  componentDidUpdate() {
    this.canvas.renderAll();
  }

  onKeyUp(event: KeyboardEvent<any>) {
    console.log("Key pressed", event);
    event.persist();
  }

  /** Writes the underlying canvas object to an .SVG format, and returns the string representing it. */
  dumpToSVG() {
    return this.canvas.toSVG({
      encoding:         "",
      viewBox:          {
        x:      0,
        y:      0,
        width:  this.canvas.getWidth(),
        height: this.canvas.getHeight()
      },
      suppressPreamble: true
    });
  }

  render() {
    return (
        <div tabIndex={0} onKeyUp={this.onKeyUp}>
          <canvas width={this.props.width}
                  height={this.props.height}
                  ref={element => this.element = element}>
            {this.props.scene.elements.map((element, index) => SceneNode.build(index, element, () => this.canvas))}
          </canvas>
        </div>
    );
  }
}
