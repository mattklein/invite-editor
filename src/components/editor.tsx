import * as React from "react";
import { Scene } from "../model/scene";

/** An editor for the actively selected element in the scene. */
export default class Editor extends React.Component<{ scene: Scene }> {
  render() {
    return (<span>Editor</span>);
  }
}