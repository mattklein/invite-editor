import { shallow } from "enzyme";
import * as React from "react";
import Canvas from "./canvas";

describe("the <Canvas/> element", () => {
  it("should render ok", () => {
    let scene = {elements: []};
    let wrapper = shallow(<Canvas scene={scene} width={100} height={100}/>);

    expect(wrapper).toBeTruthy();
  });
});