import { shallow } from "enzyme";
import * as React from "react";
import App from "./app";

describe("the <App/> element", () => {
  it("should render ok", () => {
    let wrapper = shallow(<App/>);
    expect(wrapper).toBeTruthy();
  });
});
