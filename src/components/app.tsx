import * as React from "react";
import { download } from "../common/utils";
import bus from "../model/bus";
import { Scene } from "../model/scene";
import scenes from "../model/store";
import "./app.css";
import Canvas from "./canvas";
import Editor from "./editor";
import Palette from "./palette";

const logo = require("../logo.svg");

/** The top-level application component. */
export default class App extends React.Component<{}, { scene: Scene }> {
  private canvas: Canvas | null;

  constructor() {
    super();

    this.state = {
      scene: {
        elements: []
      }
    };

    this.downloadToSVG = this.downloadToSVG.bind(this);
    this.downloadToJSON = this.downloadToJSON.bind(this);

    // listen for message bus events
    bus.take("UPDATE_ELEMENT", (message: any) => {
      // update existing elements in-place in the scene
      this.setState((state) => {
        state.scene.elements[message.index] = message.element;
        return state;
      });
    });
  }

  async componentWillMount() {
    // load initial state from the store
    this.setState({
      scene: await scenes.load()
    });
  }

  /** Downloads a copy of the canvas to .SVG format. */
  downloadToSVG() {
    if (this.canvas) {
      download("invite.svg", this.canvas.dumpToSVG());
    }
  }

  /** Downloads a copy of the scene to JSON format. */
  downloadToJSON() {
    download("invite.json", JSON.stringify(this.state.scene));
  }

  render() {
    // TODO: use a CSS grid layout for this?
    return (
        <div className="App">
          <div className="App-header">
            <img src={logo} className="App-logo" alt="logo"/>
            <h2>Prototype</h2>
          </div>
          <div className="App-intro">
            <div><Palette scene={this.state.scene}/></div>
            <div><Editor scene={this.state.scene}/></div>
            <div><button onClick={this.downloadToSVG}>Download to SVG</button></div>
            <div><button onClick={this.downloadToJSON}>Download to JSON</button></div>
            <Canvas scene={this.state.scene} width={1000} height={1000} ref={element => this.canvas = element}/>
          </div>
        </div>
    );
  }
}
