import * as React from "react";
import { loadImageAsync } from "../common/utils";
import bus from "../model/bus";
import { ImageElement, SceneElement, TextElement } from "../model/scene";

// TODO: new terminology: SceneModel and CanvasModel.
// TODO: configure redux and push changes to a redux store?
// TODO: how to get the active element (for deletion, etc).
// TODO: how to get changes from props back into node?
// TODO: how to get changes from node back into props?

/** Encapsulates the properties for a {@link SceneNode}. */
interface NodeProps<E> {
  index: number;
  element: E;
  canvasProvider: () => fabric.Canvas;
}

/** Encapsulates the state for a {@link SceneNode}  */
interface NodeState<O extends fabric.Object> {
  /** The underlying fabricJS canvas object for the node. */
  canvasObject: O;
}

/**
 * Represents one of the possible scene nodes constructed from a scene representation.
 * Each node is responsible for a single fabricJS object in the fabricJS canvas.
 * State changes to the node should replicate through to the underlying fabricJS object and vice versa.
 */
export abstract class SceneNode<E extends SceneElement, O extends fabric.Object>
    extends React.Component<NodeProps<E>, NodeState<O>> {
  /** Creates the React element from the given scene element and a provider for a canvas. */
  public static build(index: number, element: SceneElement, canvasProvider: () => fabric.Canvas) {
    return React.createElement(this.select(element) as React.ComponentClass<any>, {
      key:            index,
      index:          index,
      element:        element,
      canvasProvider: canvasProvider,
    });
  }

  /** Selects the type of node to create for the given scene element. */
  private static select(element: SceneElement) {
    switch (element.type) {
      case "text":
        return TextNode;
      case "image":
        return ImageNode;
      default:
        throw new Error("An unrecognized element was requested: " + element);
    }
  }

  async componentDidMount() {
    // after we're mounted in the DOM, create the associated fabricJS object and retain it for future updates
    const canvasObject = await this.createCanvasObject(this.props.element);

    // listen for changes in the canvas object and propagate them back to our scene model
    canvasObject.on("modified", async () => {
      await this.updateDomainObject(canvasObject, this.props.element);

      // TODO: why even do this? just visit all the scene nodes at save time instead?
      // TODO: possibly because of property editor?
      bus.send("UPDATE_ELEMENT", {
        index:   this.props.index,
        element: this.props.element
      });
    });

    this.setState({canvasObject: canvasObject}, () => {
      this.props.canvasProvider().add(canvasObject);
    });
  }

  componentWillUnmount() {
    if (this.state.canvasObject) {
      this.state.canvasObject.remove();
    }
  }

  async componentDidUpdate() {
    if (this.state.canvasObject) {
      await this.updateCanvasObject(this.state.canvasObject, this.props.element);
    }
  }

  render() {
    return (null); // technically, we're only using React for change propagation at this point.
  }

  abstract createCanvasObject(element: E): Promise<O>;
  abstract updateCanvasObject(object: O, element: E): Promise<void>;
  abstract updateDomainObject(object: O, element: E): Promise<void>;
}

/** A {@link SceneNode} that just displays text. */
class TextNode extends SceneNode<TextElement, fabric.Text> {
  // TODO: can i make these property bindings automatic somehow?

  async createCanvasObject(element: TextElement) {
    return new fabric.Text(element.text, {
      left:     element.x,
      top:      element.y,
      width:    element.width,
      height:   element.height,
      fontSize: element.fontSize
    });
  }

  async updateCanvasObject(object: fabric.Text, element: TextElement) {
    object.setLeft(element.x);
    object.setTop(element.y);
    object.setWidth(element.width);
    object.setHeight(element.height);
    object.setText(element.text);
    object.setFontSize(element.fontSize);
  }

  async updateDomainObject(object: fabric.Text, element: TextElement) {
    element.x = object.getLeft();
    element.y = object.getTop();
    element.width = object.getWidth();
    element.height = object.getHeight();
    element.text = object.getText();
    element.fontSize = object.getFontSize();
  }
}

/** A {@link SceneNode} that just displays an image. */
class ImageNode extends SceneNode<ImageElement, fabric.Image> {
  async createCanvasObject(element: ImageElement) {
    let image = await loadImageAsync(element.url, element.width, element.height);
    return new fabric.Image(image, {
      left:   element.x,
      top:    element.y,
      width:  element.width,
      height: element.height
    });
  }

  async updateCanvasObject(object: fabric.Image, element: ImageElement) {
    object.setLeft(element.x);
    object.setTop(element.y);
    object.setWidth(element.width);
    object.setHeight(element.height);
  }

  async updateDomainObject(object: fabric.Image, element: ImageElement) {
    element.x = object.getLeft();
    element.y = object.getTop();
    element.width = object.getWidth();
    element.height = object.getHeight();
  }
}
