import { shallow } from "enzyme";
import * as React from "react";
import Palette from "./palette";

describe("the <Palette/> element", () => {
  it("should render ok", () => {
    let scene = {elements: []};
    let wrapper = shallow(<Palette scene={scene}/>);

    expect(wrapper).toBeTruthy();
  });
});