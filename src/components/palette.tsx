import * as React from "react";
import { Scene } from "../model/scene";

/** A palette of items to be dragged onto the canvasProvider. */
export default class Palette extends React.Component<{ scene: Scene }> {
  render() {
    return (<span>Palette</span>);
  }
}