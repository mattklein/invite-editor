/** Represents some store for a type, T. */
import { Scene } from "./scene";

class SceneStore {
  async load(): Promise<Scene> {
    // TODO: load this from somewhere
    return {
      elements: [
        {type: "text", x: 100, y: 0, width: 100, height: 100, fontSize: 24, text: "This is a message!"},
        {type: "image", x: 300, y: 100, width: 120, height: 88, url: "https://goo.gl/NRMPJg"}
      ]
    };
  }

  async save(scene: Scene): Promise<void> {
    // TODO: save this to somewhere
    throw new Error("Not yet implemented");
  }
}

const scenes = new SceneStore();

export default scenes;