/** Common properties for all {@link SceneElement}s. */
interface CommonElement {
  x: number;
  y: number;
  width: number;
  height: number;
}

/** Represents text in some {@link Scene}. */
export interface TextElement extends CommonElement {
  type: "text";
  text: string;
  fontSize: number;
}

/** Represents an image in some {@link Scene}. */
export interface ImageElement extends CommonElement {
  type: "image";
  url: string;
}

/** A union of all the possible types of elements in a {@link Scene}. */
export type SceneElement = TextElement | ImageElement;

/** Represents a scene that may be rendered to a canvas. */
export interface Scene {
  elements: SceneElement[];
}