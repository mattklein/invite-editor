import { createBus, MessageBus } from "suber";

/** We use a singleton bus for passing messages up/down the application components. */
const bus: MessageBus = createBus();

export default bus;