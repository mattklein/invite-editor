/** Causes a file download for a file with the given name and plaintext. */
export function download(filename: string, text: string) {
  // a dirty hack that creates an anchor on the page with some data:text in the link.
  // seems the most reliable way of getting the browser to download via html/javascript.
  const element = document.createElement("a");

  element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
  element.setAttribute("download", filename);

  element.style.display = "none";
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

/** Loads an image from the given url into a new img element with the given width and height. */
export function loadImageAsync(url: string, width?: number, height?: number): Promise<HTMLImageElement> {
  return new Promise((resolve, reject) => {
    const element = new Image(width, height);

    element.onload = () => resolve(element);
    element.onerror = (error) => reject(error);
    element.src = url;

    return element;
  });
}
