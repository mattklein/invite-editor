import * as __fabric from "fabric";

// What's this? it's a workaround for some weird UMD nonsense in TypeScript:
// see https://github.com/Microsoft/TypeScript/issues/10178
//
// Basically, we're using fabricJS as a pre-packaged library, as the NPM version requires native extensions to build.
//
// In order to get this to work with TypeScript's .d.ts files (for better intellisense) we need to use this hack.
declare global {
  // noinspection JSUnusedGlobalSymbols
  const fabric: typeof __fabric;
}
