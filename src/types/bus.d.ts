declare module "suber" {
  export interface MessageBus {
    take(event: string, handler: (message: any) => void): void;
    send(event: string, message: any): void;
  }

  export function createBus(): MessageBus;
}