// load polyfills
require("raf/polyfill");

// configure enzyme adapter
let enzyme = require("enzyme");
let adapter = require("enzyme-adapter-react-16");
enzyme.configure({ adapter: new adapter() });
