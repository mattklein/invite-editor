import * as React from "react";
import * as ReactDOM from "react-dom";
import "./common/fabric";
import App from "./components/app";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

// TODO: add a switch to determine if this should be editor mode vs final rendered mode?

ReactDOM.render(
    <App/>,
    document.getElementById("root") as HTMLElement
);

registerServiceWorker();
